/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.cars.sales;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import cc.cars.BadDataException;
import cc.cars.Car;

/**
Test for the {@link HardNosed HardNosed salesman}.

@author Will Provost
*/
public class HardNosedTest
{
    /**
    Factory method for a car to sell.
    */
    public static Car getCarOfMyDreams ()
        throws BadDataException
    {
        return new Car ("", "", 2000, "", "", 10000, 0, 0, Car.Handling.GOOD);
    }
    
    /**
    Helper method to assert that we can conduct a negotiation that leads
    to a deal.
    */
    public static void assertSaleSuccess (HardNosed salesman)
    {
        salesman.getAskingPrice ();
        assertEquals (8900.00, salesman.willSellAt (8500), .001); // down 20%
        assertEquals (8820.00, salesman.willSellAt (8500), .001); // down 20%
        assertEquals (8756.00, salesman.willSellAt (8500), .001); // down 20%
        assertEquals (8756.00, salesman.willSellAt (8000), .001); // no, you don't!
        assertEquals (8704.80, salesman.willSellAt (8500), .001); // down 20%
        assertEquals (8663.84, salesman.willSellAt (8500), .001); // down 20%
        assertEquals (8665, salesman.willSellAt (8665), .001);    // sold!
    }
    
    private Car carOfMyDreams;
    private HardNosed salesman;
    
    /**
    Initialize the target car and a standard salesman to sell it.
    */
    @Before
    public void setUp ()
        throws Exception
    {
        carOfMyDreams = getCarOfMyDreams ();
        salesman = new HardNosed (carOfMyDreams);
    }
    
    /**
    Check the asking price.
    */
    @Test
    public void testStandardDiscounts ()
        throws Exception
    {
        assertEquals (9000, salesman.getAskingPrice (), .001);
    }
    
    /**
    Test a successful negotiation.
    */
    @Test
    public void testSaleSuccess ()
        throws Exception
    {
        assertSaleSuccess (salesman);
    }
    
    /**
    Test a failed negotiation.
    */
    @Test
    public void testSaleNoMovement ()
        throws Exception
    {
        salesman.getAskingPrice ();
        assertEquals (9000, salesman.willSellAt (8000), .001);
        assertEquals (9000, salesman.willSellAt (8200), .001);
    }
}
