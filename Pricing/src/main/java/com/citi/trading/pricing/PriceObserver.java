package com.citi.trading.pricing;

import java.util.List;

public interface PriceObserver {
	public void updatePrice(List<PricePoint> newPrices);
	public String getStock();
	
}

