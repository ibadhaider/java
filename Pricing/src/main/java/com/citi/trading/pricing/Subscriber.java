package com.citi.trading.pricing;

import java.util.ArrayList;
import java.util.List;

public class Subscriber implements PriceObserver {
	private String stock;
	private List<PricePoint> currentPrices;
	public Subscriber (String stock) {
		this.stock = stock;
		this.currentPrices = new ArrayList<PricePoint>();
	}
	
	@Override
	public void updatePrice(List<PricePoint> newPrices) {
		// TODO Auto-generated method stub
		this.currentPrices = newPrices;
		System.out.println("Prices changed to" + this.currentPrices);
	}

	@Override
	public String getStock() {
		// TODO Auto-generated method stub
		return this.stock;
	}

}
